package org.larskristian.zatacka.geometry;

public class Line {
    public Point from;
    public Point to;

    public Line(short fromX, short fromY, short toX, short toY) {
        from = new Point(fromX, fromY);
        to = new Point(toX, toY);
    }

}
