package org.larskristian.zatacka.gui;

import org.larskristian.zatacka.CustomView;

import android.content.Context;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

public class ZatackaButton extends Button {

    private CustomView cv;

    public ZatackaButton(Context context, String name, int resource,
            CustomView cv) {
        super(context);
        setTextSize(30);
        setBackgroundResource(resource);
        this.cv = cv;
        if (name.equals("Left")) {
            addLeftListener();
        } else {
            addRightListener();
        }
    }

    private void addLeftListener() {
        setOnTouchListener(new OnTouchListener() {
            private boolean number6Pressed = false;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                synchronized (cv) {
                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        cv.turnLeft = true;
                    } else if (event.getAction() == MotionEvent.ACTION_POINTER_1_DOWN) {
                        cv.turnLeft = true;
                        number6Pressed = false;
                    } else if (event.getAction() == MotionEvent.ACTION_POINTER_2_DOWN) {
                        cv.turnRight = true;
                    } else if (event.getAction() == MotionEvent.ACTION_UP) {
                        if (!number6Pressed) {
                            cv.turnLeft = false;
                        } else {
                            cv.turnRight = false;
                            number6Pressed = false;
                        }
                    } else if (event.getAction() == MotionEvent.ACTION_POINTER_1_UP) {
                        cv.turnLeft = false;
                        number6Pressed = true;
                    } else if (event.getAction() == MotionEvent.ACTION_POINTER_2_UP) {
                        cv.turnRight = false;
                    }
                    return false;
                }
            }
        });
    }

    private void addRightListener() {
        setOnTouchListener(new OnTouchListener() {
            private boolean number6Pressed = false;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                synchronized (cv) {
                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        cv.turnRight = true;
                    } else if (event.getAction() == MotionEvent.ACTION_POINTER_1_DOWN) {
                        cv.turnRight = true;
                        number6Pressed = false;
                    } else if (event.getAction() == MotionEvent.ACTION_POINTER_2_DOWN) {
                        cv.turnLeft = true;
                    } else if (event.getAction() == MotionEvent.ACTION_UP) {
                        if (!number6Pressed) {
                            cv.turnRight = false;
                        } else {
                            cv.turnLeft = false;
                            number6Pressed = false;
                        }
                    } else if (event.getAction() == MotionEvent.ACTION_POINTER_1_UP) {
                        cv.turnRight = false;
                        number6Pressed = true;
                    } else if (event.getAction() == MotionEvent.ACTION_POINTER_2_UP) {
                        cv.turnLeft = false;
                    }
                    return false;
                }
            }
        });
    }

}
