package org.larskristian.zatacka;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.larskristian.zatacka.geometry.Line;
import org.larskristian.zatacka.geometry.Point;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.os.Vibrator;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class CustomView extends SurfaceView implements SurfaceHolder.Callback {

    private final short maxXValue = 800;
    private final short maxYValue = 800;
    private final short turning = 6;
    private final short ticksBetweenHoles = 100;
    private final short ticksInHoles = 4;

    private short oldXValue = maxXValue / 2;
    private short oldYValue = maxYValue / 2;
    private short newXValue = maxXValue / 2;
    private short newYValue = maxYValue / 2;
    private float angle = (float) (Math.PI / 2);
    private short currentTickBetweenHoles = (short) (Math.random() * ticksBetweenHoles);
    private boolean newHole = false;

    private Line line;
    private int A1;
    private int B1;
    private int C1;
    private int A2;
    private int B2;
    private int C2;
    private int det;

    private double x;
    private double y;

    private int tempSmallXValue1;
    private int tempLargeXValue1;
    private int tempSmallYValue1;
    private int tempLargeYValue1;
    private int tempSmallXValue2;
    private int tempLargeXValue2;
    private int tempSmallYValue2;
    private int tempLargeYValue2;

    private int points;
    private Timer timer;
    private Paint mPaint;
    private Paint blankPaint;
    private Paint textPaint;
    private List<Path> pathList = new ArrayList<Path>();
    private Path currentPath;
    private Canvas c;
    private boolean start = true;

    private short restart = 0;

    public boolean turnLeft = false;
    public boolean turnRight = false;

    private LinkedList<Line> lineList = new LinkedList<Line>();
    private Line nextLine = null;
    private Point point;
    private ZatackaAct za;

    public CustomView(ZatackaAct za) {
        super(za);
        this.za = za;
        currentPath = new Path();
        setupPaint();
        getHolder().addCallback(this);
    }

    private void addToPath() {
        if (start) {
            currentPath.moveTo(((float) oldXValue)
                    * ((float) getWidth() / (float) maxXValue),
                    ((float) oldYValue)
                            * ((float) getHeight() / (float) maxYValue));
            start = false;
        }
        if (newHole) {
            newHole = false;
            pathList.add(currentPath);
            currentPath = new Path();
            currentPath.moveTo(((float) oldXValue)
                    * ((float) getWidth() / (float) maxXValue),
                    ((float) oldYValue)
                            * ((float) getHeight() / (float) maxYValue));
        } else {
            if (currentTickBetweenHoles == 0) {
                newHole = true;
                currentTickBetweenHoles = ticksBetweenHoles;
            } else {
                currentTickBetweenHoles--;
            }
            currentPath.lineTo(((float) newXValue)
                    * ((float) getWidth() / (float) maxXValue),
                    ((float) newYValue)
                            * ((float) getHeight() / (float) maxYValue));
        }
    }

    public float getAngle() {
        return angle;
    }

    public void onClockTick() {
        points++;
        if (turnLeft && !turnRight) {
            setAngle((float) (getAngle() - Math.PI / 32));
        } else if (turnRight && !turnLeft) {
            setAngle((float) (getAngle() + Math.PI / 32));
        }
        newXValue += (int) (turning * Math.cos(angle));
        newYValue += (int) (turning * Math.sin(angle));

        if (newHole) {
            newXValue += (int) (turning * Math.cos(angle) * ticksInHoles);
            newYValue += (int) (turning * Math.sin(angle) * ticksInHoles);
            oldXValue += (int) (turning * Math.cos(angle) * ticksInHoles);
            oldYValue += (int) (turning * Math.sin(angle) * ticksInHoles);
        }

        // Log.i("Zatacka", "skal teste, satt nye verdier: x; " + newXValue +
        // " y; " + newYValue);

        if (validMove()) { // Smud!
            if (nextLine != null) {
                synchronized (lineList) {
                    lineList.add(nextLine);
                    // Log.i("Zatacka", "suksess");
                }
            }
            nextLine = new Line(oldXValue, oldYValue, newXValue, newYValue);
            oldXValue = newXValue;
            oldYValue = newYValue;
            addToPath();
            updateGUI();
        } else { // Fail!
            // Log.i("Zatacka", "fail");
            Vibrator v = (Vibrator) za
                    .getSystemService(Context.VIBRATOR_SERVICE);
            v.vibrate(200);
            timer.cancel();
            za.handler.post(za.showDialog);
        }
    }

    @Override
    public void onDraw(Canvas canvas) {
        canvas.drawRect(0, 0, 100, 20, blankPaint);
        if (restart > 0) {
            canvas.drawPaint(blankPaint);
            restart--;
        } else {
            for (int i = 0; i < pathList.size(); i++) {
                canvas.drawPath(pathList.get(i), mPaint);
            }
            canvas.drawPath(currentPath, mPaint);
            canvas.drawText("" + points, 0, 10, textPaint);
        }
    }

    public void restartLevel() {
        timer.cancel();
        restart = 2;
        start = true;
        oldXValue = maxXValue / 2;
        oldYValue = maxYValue / 2;
        newXValue = maxXValue / 2;
        newYValue = maxYValue / 2;
        lineList.clear();
        nextLine = null;
        angle = (float) (Math.PI / 2);
        currentPath = new Path();
        currentPath.moveTo(oldXValue, oldYValue);
        pathList.clear();

        turnLeft = false;
        turnRight = false;
        points = 0;

        startTimer();
    }

    public void setAngle(float angle) {
        this.angle = angle;
    }

    private void setupPaint() {
        mPaint = new Paint();
        mPaint.setDither(true);
        mPaint.setColor(0xFFFFFF00);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setStrokeWidth(6);

        textPaint = new Paint();
        textPaint.setDither(true);
        textPaint.setColor(0xFF00FF00);
        textPaint.setStyle(Paint.Style.FILL);
        textPaint.setStrokeJoin(Paint.Join.ROUND);
        textPaint.setStrokeCap(Paint.Cap.ROUND);
        textPaint.setStrokeWidth(2);

        blankPaint = new Paint();
    }

    private void sortNumbers() {
        if (newXValue > oldXValue) {
            tempSmallXValue1 = oldXValue;
            tempLargeXValue1 = newXValue;
        } else {
            tempSmallXValue1 = newXValue;
            tempLargeXValue1 = oldXValue;
        }
        if (newYValue > oldYValue) {
            tempSmallYValue1 = oldYValue;
            tempLargeYValue1 = newYValue;
        } else {
            tempSmallYValue1 = newYValue;
            tempLargeYValue1 = oldYValue;
        }
        if (line.to.x > line.from.x) {
            tempSmallXValue2 = line.from.x;
            tempLargeXValue2 = line.to.x;
        } else {
            tempSmallXValue2 = line.to.x;
            tempLargeXValue2 = line.from.x;
        }
        if (line.to.y > line.from.y) {
            tempSmallYValue2 = line.from.y;
            tempLargeYValue2 = line.to.y;
        } else {
            tempSmallYValue2 = line.to.y;
            tempLargeYValue2 = line.from.y;
        }
    }

    private void startTimer() {
        timer = new Timer("Snake");
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                onClockTick();
            }
        }, 0, 40);
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width,
            int height) {
    }

    public void surfaceCreated(SurfaceHolder holder) {
        startTimer();
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
    }

    private void updateGUI() {
        c = null;
        try {
            c = getHolder().lockCanvas(null);
            synchronized (getHolder()) {
                onDraw(c);
            }
        } finally {
            if (c != null) {
                getHolder().unlockCanvasAndPost(c);
            }
        }
    }

    private boolean validMove() {
        synchronized (lineList) {
            // Log.i("Zatacka", "min linje: x1; " + oldXValue + ", y1; " +
            // oldYValue + ", x2; " + newXValue + ", y2; " + newYValue);
            // Log.i("Zatacka", "antall i treet: " + tree.size());
            if (newYValue > maxYValue || newYValue < 0 || newXValue > maxXValue
                    || newXValue < 0) {
                return false;
            }
            Iterator<Line> lineSet = lineList.iterator();
            while (lineSet.hasNext()) {
                line = lineSet.next();
                point = line.to;
                if (newXValue - 3 * turning < point.x
                        && point.x < newXValue + 3 * turning
                        && newYValue - 3 * turning < point.y
                        && point.y < newYValue + 3 * turning) {
                    // Log.i("Zatacka", "Punkt i keySet: x; " + point.x + " y; "
                    // + point.y + " ( x1;" + point.line.from.x + " y1;" +
                    // point.line.from.y + " x2;" + point.line.to.x + " y2;" +
                    // point.line.to.y + " )");
                    A1 = newYValue - oldYValue;
                    B1 = oldXValue - newXValue;
                    C1 = A1 * oldXValue + B1 * oldYValue;

                    A2 = line.to.y - line.from.y;
                    B2 = line.from.x - line.to.x;
                    C2 = A2 * line.from.x + B2 * line.from.y;

                    det = A1 * B2 - A2 * B1;
                    if (det != 0) { // Ikke parallelle linjer
                        // Log.i("Zatacka", "ikke parallelle linjer");
                        x = (B2 * C1 - B1 * C2) / det;
                        y = (A1 * C2 - A2 * C1) / det;
                        sortNumbers();
                        if ((tempSmallXValue1 <= x && x <= tempLargeXValue1)
                                && (tempSmallXValue2 <= x && x <= tempLargeXValue2)
                                && (tempSmallYValue1 <= y && y <= tempLargeYValue1)
                                && (tempSmallYValue2 <= y && y <= tempLargeYValue2)) {
                            return false;
                        }
                    }
                }
            }
            // Log.i("Zatacka", "Move var valid");
            return true;
        }
    }
}