package org.larskristian.zatacka;

import org.larskristian.zatacka.gui.ZatackaButton;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;

public class ZatackaAct extends Activity {
    private CustomView cv;
    public final Handler handler = new Handler();

    private LinearLayout gameLayout;
    private LinearLayout menuLayout;

    public final Runnable showDialog = new Runnable() {
        public void run() {
            Dialog dialog;
            AlertDialog.Builder builder = new AlertDialog.Builder(
                    ZatackaAct.this);
            builder.setMessage("Nooooob^2k! Retry?")
                    .setCancelable(false)
                    .setPositiveButton("Yes",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                        int id) {
                                    cv.restartLevel();
                                }
                            })
                    .setNegativeButton("No",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                        int id) {
                                    System.exit(0);
                                }
                            });
            dialog = builder.create();
            dialog.show();
        }
    };

    private void initializeGame() {
        gameLayout = new LinearLayout(this);
        gameLayout.setOrientation(LinearLayout.VERTICAL);
        gameLayout.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,
                LayoutParams.FILL_PARENT));
        cv = new CustomView(this);
        LinearLayout ll = new LinearLayout(this);
        ll.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,
                LayoutParams.WRAP_CONTENT));
        ll.setOrientation(LinearLayout.HORIZONTAL);
        ZatackaButton bleft = new ZatackaButton(this, "Left",
                R.drawable.leftarrow, cv);
        ZatackaButton bright = new ZatackaButton(this, "Right",
                R.drawable.rightarrow, cv);
        ll.addView(bleft, getWindow().getWindowManager().getDefaultDisplay()
                .getWidth() / 2, getWindow().getWindowManager()
                .getDefaultDisplay().getHeight() * 1 / 5);
        ll.addView(bright, getWindow().getWindowManager().getDefaultDisplay()
                .getWidth() / 2, getWindow().getWindowManager()
                .getDefaultDisplay().getHeight() * 1 / 5);
        gameLayout.addView(cv, getWindow().getWindowManager()
                .getDefaultDisplay().getWidth(), getWindow().getWindowManager()
                .getDefaultDisplay().getHeight() * 4 / 5);
        gameLayout.addView(ll, getWindow().getWindowManager()
                .getDefaultDisplay().getWidth(), getWindow().getWindowManager()
                .getDefaultDisplay().getHeight() * 1 / 5);
        setContentView(gameLayout);
    }

    private void initializeMenu() {
        menuLayout = new LinearLayout(this);
        menuLayout.setOrientation(LinearLayout.VERTICAL);
        menuLayout.setGravity(Gravity.BOTTOM);
        menuLayout.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,
                LayoutParams.FILL_PARENT));
        menuLayout.setBackgroundResource(R.drawable.frontpage);

        Button host = new Button(this);
        host.setText("Host Game");
        host.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                initializeGame();
            }

        });
        Button join = new Button(this);
        join.setText("Join Game");

        menuLayout.addView(host);
        menuLayout.addView(join);

        setContentView(menuLayout);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        // getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
        // WindowManager.LayoutParams.FLAG_FULLSCREEN );
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().clearFlags(
                WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
        initializeMenu();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        if (item.getItemId() == R.id.menu_item1) {
            Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            v.vibrate(600);
        } else if (item.getItemId() == R.id.menu_item2) {
            System.exit(0);
        } else if (item.getItemId() == R.id.menu_item3) {
            cv.restartLevel();
        }
        return false;
    }
}
